from django.urls import path
from receipts.views import get_receipts_list


urlpatterns = [
    path("", get_receipts_list, name="home")
]
